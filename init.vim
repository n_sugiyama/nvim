if has("syntax")
  syntax on
endif

let s:dein_dir = expand('~/.cache/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  let g:rc_dir    = expand('~/.config/nvim/rc')
  let s:toml      = g:rc_dir . '/dein.toml'
  let s:lazy_toml = g:rc_dir . '/dein_lazy.toml'
  let s:toml_toml = g:rc_dir . '/dein_toml.toml'

  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})
  call dein#load_toml(s:toml_toml, {'lazy': 1})

  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif

set nocompatible
filetype off
filetype plugin indent on
syntax enable

set number
set title
set ambiwidth=double
set tabstop=2
set expandtab
set shiftwidth=2
set smartindent
set nrformats-=octal
set hidden
set history=50
set virtualedit=block
set whichwrap=b,s,[,],<,>
set backspace=indent,eol,start
set wildmenu
set t_Co=256

" For clipboard
set clipboard+=unnamedplus

" For tags
nnoremap <C-]> g<C-]>
set fileformats=unix,dos,mac
set fileencodings=utf-8,sjis

set tags=./tags;$HOME


" For cursor line
set cursorline
highlight CursorLine cterm=underline ctermfg=None ctermbg=NONE

autocmd ColorScheme * highlight Comment ctermfg=102
autocmd ColorScheme * highlight Visual ctermbg=236
colorscheme molokai

" For python path
let g:python_host_prob = '/usr/bin/python2.7'
if has('mac')
  let g:python3_host_prog = '/usr/local/bin/python3'
else
  let g:python3_host_prog = '/usr/bin/python3'
endif
